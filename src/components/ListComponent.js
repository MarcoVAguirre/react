import React, {Component} from 'react';

class ListComponent extends Component
{
    
    render()
    {
        var arrList = ["Learn React", "Repair Autoit Script", "Investigate API in C#"]
        
        function ShowListOfTasksToDo(arrList) 
        {
            let contentToShowOnScreen = 
            (
                <div>
                    <ul>
                        {arrList.map((task, i) =>
                        {
                            return(
                            <li key={i}>{task}</li>)
                        })}
                    </ul>
                </div>
            )
            
            return contentToShowOnScreen;
        }

        return(
            <div> {ShowListOfTasksToDo(arrList)} </div>)
    }
}

export default ListComponent;