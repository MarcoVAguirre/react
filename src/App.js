import React from 'react';
import logo from './logo.svg';
import './App.css';
import ListComponent from './components/ListComponent';

function SayHello(strPersonWhoGreet) 
{
  var Greeting = 
  (
    <h1>{strPersonWhoGreet} says hello Luis</h1>
  )

  return Greeting
}

function App() 
{
  var name = "Marco"
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <div>
          {SayHello(name)}
          <p>And this are the tasks that {name} have to do</p>
          <ListComponent/>
        </div>
        <div>

        </div>
      </header>
    </div>
  );
}

export default App;